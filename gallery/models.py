from django.db import models

from stdimage import StdImageField


class PlantGallery(models.Model):
    image = StdImageField(upload_to='plants', variations={
        'detail': (800, 600, True),
        'thumbnail': (300, 200, True),
    }, delete_orphans=True)


class DiseaseGallery(models.Model):
    image = StdImageField(upload_to='diseases', variations={
        'detail': (800, 600, True),
        'thumbnail': (300, 200, True),
    }, delete_orphans=True)
