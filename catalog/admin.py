from django.contrib import admin

from catalog.models import *


# Register your models here.
admin.site.register(Month)
admin.site.register(Clime)
admin.site.register(Category)
admin.site.register(Plant)
admin.site.register(PopularName)
admin.site.register(LifeCycle)
admin.site.register(Lightness)
admin.site.register(Syntrophy)
admin.site.register(Plantation)
admin.site.register(Region)
admin.site.register(PlantVariety)
admin.site.register(CarbonCycle)
admin.site.register(Water)
admin.site.register(MonthsRegion)
admin.site.register(MonthDetail)
admin.site.register(DiseaseAffect)
admin.site.register(Disease)
admin.site.register(Classe)
admin.site.register(Order)
admin.site.register(Family)
admin.site.register(Gender)
