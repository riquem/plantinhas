from django.contrib.auth.models import User
from django.db import models

from simple_history.models import HistoricalRecords

from gallery.models import PlantGallery, DiseaseGallery
# https://django-simple-history.readthedocs.io/en/latest/

# use it to get users feedback of the site in future
# https://github.com/ludrao/django-tellme

# to auth user with api
# https://djangopackages.org/packages/p/djangorestframework-api-key/

# stdimage
# https://github.com/codingjoe/django-stdimage

# to future blog
# http://docs.django-blog-zinnia.com/en/latest/index.html


class Category(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField()
    history = HistoricalRecords()

    def __str__(self):
        return self.name


class Month(models.Model):
    name = models.CharField(max_length=12)
    number = models.IntegerField()

    def __str__(self):
        return self.name


class Clime(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField()
    history = HistoricalRecords()

    def __str__(self):
        return self.name


class Region(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField(blank=True)
    clime = models.ForeignKey(Clime, on_delete=models.SET_NULL, null=True)
    history = HistoricalRecords()

    def __str__(self):
        return self.name


class LifeCycle(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField(blank=True)
    history = HistoricalRecords()

    def __str__(self):
        return self.name


class Lightness(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField(blank=True)
    history = HistoricalRecords()

    def __str__(self):
        return self.name


class Water(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField(blank=True)
    history = HistoricalRecords()

    def __str__(self):
        return self.name


class Disease(models.Model):
    name = models.CharField(max_length=50)
    scientific_name = models.CharField(max_length=50)
    description = models.TextField(blank=True)
    history = HistoricalRecords()

    def __str__(self):
        return self.name


class CarbonCycle(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField(blank=True)
    history = HistoricalRecords()

    def __str__(self):
        return self.name


class DiseaseAffect(models.Model):
    disease = models.ForeignKey(Disease, on_delete=models.CASCADE)
    gallery = models.ManyToManyField(DiseaseGallery, blank=True)


class Classe(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField(blank=True)

    def __str__(self):
        return self.name


class Order(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField(blank=True)
    classe = models.ForeignKey(Classe, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Family(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField(blank=True)
    order = models.ForeignKey(Order, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Gender(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField(blank=True)
    family = models.ForeignKey(Family, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Plant(models.Model):
    scientific_name = models.CharField(max_length=50)
    slug = models.SlugField(max_length=55)
    description = models.TextField()
    gender = models.ForeignKey(Gender, on_delete=models.SET_NULL, blank=True, null=True)
    categories = models.ManyToManyField(Category, blank=True)
    life_cycle = models.ForeignKey(LifeCycle, on_delete=models.SET_NULL, null=True)
    carbon_cycle = models.ForeignKey(CarbonCycle, on_delete=models.SET_NULL, null=True)
    lightness = models.ForeignKey(Lightness, on_delete=models.SET_NULL, null=True)
    climes = models.ManyToManyField(Clime, blank=True)
    water = models.ForeignKey(Water, on_delete=models.SET_NULL, null=True)
    diseases = models.ManyToManyField(DiseaseAffect, related_name='plant', blank=True)
    gallery = models.ManyToManyField(PlantGallery, blank=True)
    history = HistoricalRecords()

    def __str__(self):
        return self.scientific_name


class PlantVariety(models.Model):
    variety_name = models.CharField(max_length=50)
    slug = models.SlugField(max_length=55)
    plant = models.ForeignKey(Plant, related_name='varieties', on_delete=models.CASCADE)
    description = models.TextField()
    life_cycle = models.ForeignKey(LifeCycle, on_delete=models.SET_NULL, null=True)
    carbon_cycle = models.ForeignKey(CarbonCycle, on_delete=models.SET_NULL, null=True)
    lightness = models.ForeignKey(Lightness, on_delete=models.SET_NULL, null=True)
    climes = models.ManyToManyField(Clime, blank=True)
    diseases = models.ManyToManyField(DiseaseAffect, blank=True)
    gallery = models.ManyToManyField(PlantGallery, blank=True)
    history = HistoricalRecords()

    def __str__(self):
        return str(self.plant) + " var. " + str(self.variety_name)


class Indicator(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField(blank=True)
    plant = models.ForeignKey(Plant, on_delete=models.CASCADE, blank=True)
    variety = models.ForeignKey(PlantVariety, on_delete=models.CASCADE, blank=True)
    history = HistoricalRecords()

    def __str__(self):
        return self.name


class Syntrophy(models.Model):
    plant1 = models.ForeignKey(Plant, related_name='me', on_delete=models.CASCADE)
    plant2 = models.ForeignKey(Plant, related_name='to', on_delete=models.CASCADE)
    description = models.TextField(blank=True)
    positive = models.BooleanField(default=True)
    history = HistoricalRecords()

    def __str__(self):
        return str(self.plant1) + ' com a ' + str(self.plant2)


class Plantation(models.Model):
    plant = models.ForeignKey(Plant, related_name="plantations",
                              on_delete=models.SET_NULL, null=True, blank=True)
    variety = models.ForeignKey(PlantVariety, related_name="plantations",
                                on_delete=models.SET_NULL, null=True, blank=True)
    description = models.TextField(blank=True)
    title = models.CharField(max_length=12)

    history = HistoricalRecords()

    def __str__(self):
        return str(self.plant) + ' fase ' + str(self.title)


class PopularName(models.Model):
    popular_name = models.CharField(max_length=50)
    description = models.TextField(blank=True)
    plant = models.ForeignKey(Plant, related_name='popular_names', on_delete=models.CASCADE)
    history = HistoricalRecords()

    def __str__(self):
        return self.popular_name


class MonthsRegion(models.Model):
    region = models.ForeignKey(Region, on_delete=models.SET_NULL, null=True)

    plantation = models.ForeignKey(Plantation, related_name="months_region", on_delete=models.CASCADE)

    history = HistoricalRecords()

    def __str__(self):
        return str(self.region)


class MonthDetail(models.Model):
    month = models.ForeignKey(Month, on_delete=models.CASCADE)
    CHOICES = [('1', 'Good'),
               ('2', 'Medium'),
               ('3', 'Bad')]
    information = models.CharField(max_length=1, choices=CHOICES)
    month_region = models.ForeignKey(MonthsRegion, related_name='months', on_delete=models.CASCADE)


