from django.contrib import admin
from django.urls import path, include

from rest_framework import renderers
from rest_framework.urlpatterns import format_suffix_patterns

from .views import plants, plant_detail
from .viewsets import *


urlpatterns = format_suffix_patterns([
    path('plantas/', plants, name='plants'),
    path('plantas/<slug:slug>/', plant_detail, name='plant-detail')
])