from rest_framework import serializers

from gallery.serializers import PlantGallerySerializer, DiseaseGallerySerializer

from .models import *


class ClasseSerializer(serializers.ModelSerializer):

    class Meta:
        model = Classe
        fields = ['name', ]


class OrderSerializer(serializers.ModelSerializer):
    classe = ClasseSerializer()

    class Meta:
        model = Order
        fields = ['name', 'classe', ]


class FamilySerializer(serializers.ModelSerializer):
    order = OrderSerializer()

    class Meta:
        model = Family
        fields = ['name', 'order', ]


class GenderSerializer(serializers.ModelSerializer):
    family = FamilySerializer()

    class Meta:
        model = Gender
        fields = ['name', 'family', ]


class HistoricalRecordField(serializers.ListField):
    child = serializers.DictField()

    def to_representation(self, data):
        return super().to_representation(data.values())


class LifeCycleSerializer(serializers.ModelSerializer):
    class Meta:
        model = LifeCycle
        fields = ['name', 'id', 'description', ]


class ClimeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Clime
        fields = ['name', 'id', 'description', ]


class LightnessSerializer(serializers.ModelSerializer):
    class Meta:
        model = Lightness
        fields = ['name', 'id', 'description', ]


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ['name', 'id', 'description']


class WaterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Water
        fields = ['name', 'id', 'description', ]


class CarbonCycleSerializer(serializers.ModelSerializer):
    class Meta:
        model = CarbonCycle
        fields = ['name', 'description', ]


class DiseaseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Disease
        fields = ['name', 'description', ]


class MonthSerializer(serializers.ModelSerializer):
    class Meta:
        model = Month
        fields = ['number', 'name', ]


class RegionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Region
        fields = ['name', 'id']


class PlantVarietySerializer(serializers.ModelSerializer):
    class Meta:
        model = PlantVariety
        fields = ['variety_name', ]


class PopularNameSerializer(serializers.ModelSerializer):

    class Meta:
        model = PopularName
        fields = ['popular_name', ]


class MonthDetailSerializer(serializers.ModelSerializer):
    month = MonthSerializer()

    class Meta:
        model = MonthDetail
        fields = ['month', 'information', ]


class MonthsRegionSerializer(serializers.ModelSerializer):
    months = MonthDetailSerializer(many=True)
    region = RegionSerializer()

    class Meta:
        model = MonthsRegion
        fields = ['months', 'region']


class PlantationSerializer(serializers.ModelSerializer):
    months_region = MonthsRegionSerializer(many=True)

    class Meta:
        model = Plantation
        fields = ['months_region', 'description', 'title']


class PlantSerializer(serializers.ModelSerializer):
    popular_names = serializers.StringRelatedField(many=True)
    history = HistoricalRecordField(read_only=True)
    gallery = PlantGallerySerializer(many=True)

    class Meta:
        model = Plant
        fields = ['scientific_name', 'popular_names', 'slug', 'history', 'gallery']


class CharacteristicSerializer(serializers.ModelSerializer):
    climes = ClimeSerializer(many=True, read_only=True)
    water = WaterSerializer(read_only=True)
    life_cycle = LifeCycleSerializer()
    carbon_cycle = CarbonCycleSerializer(read_only=True)
    lightness = LightnessSerializer(read_only=True)
    varieties = PlantVarietySerializer(many=True)
    gallery = PlantGallerySerializer(many=True)
    gender = GenderSerializer()

    class Meta:
        model = Plant
        fields = ['slug', 'climes', 'water', 'carbon_cycle', 'lightness', 'life_cycle', 'varieties', 'gallery', 'gender' ]


class CharacteristicVarSerializer(serializers.ModelSerializer):
    climes = ClimeSerializer(many=True, read_only=True)
    water = WaterSerializer(read_only=True)
    life_cycle = LifeCycleSerializer(read_only=True)
    carbon_cycle = CarbonCycleSerializer(read_only=True)
    lightness = LightnessSerializer(read_only=True)
    gallery = PlantGallerySerializer(many=True)

    class Meta:
        model = PlantVariety
        fields = ['slug', 'climes', 'water', 'carbon_cycle', 'lightness', 'life_cycle', 'gallery', ]


class SyntrophySerializer(serializers.ModelSerializer):
    plant2 = PlantSerializer()

    class Meta:
        model = Syntrophy
        fields = ['plant2', 'description']


class DiseaseAffectSerializer(serializers.ModelSerializer):
    disease = DiseaseSerializer()
    gallery = DiseaseGallerySerializer(many=True)

    class Meta:
        model = DiseaseAffect
        fields = ['disease', 'gallery', ]
