from rest_framework import routers
from catalog.viewsets import *

router = routers.DefaultRouter()

router.register(r'plants', PlantViewSet)
router.register(r'characteristics', CharacteristicsViewSet)
router.register(r'characteristicsvar', CharacteristicsVarietyViewSet)
router.register(r'syntrophies', SyntrophyViewSet)
router.register(r'diseaseaffect', DiseaseAffectViewSet)
router.register(r'plantations', PlantationViewSet)
router.register(r'months', MonthViewSet)
router.register(r'regions', RegionViewSet)
router.register(r'varieties', PlantVarietyViewSet)
router.register(r'lightness', LightnessViewSet)
router.register(r'lifecycles', LifeCycleViewSet)
router.register(r'categories', CategoryViewSet)
router.register(r'climes', ClimeViewSet)
